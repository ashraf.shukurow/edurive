package az.edurive.course.model.dto.request;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Positive;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class VideoRequest {
    @NotBlank(message = "Video title should not be empty and null")
    @Schema(name = "title", example = "title how to start to learn driving", requiredMode = Schema.RequiredMode.REQUIRED)
    private String title;
    @NotBlank(message = "Video url should not be empty and null")
    @Schema(name = "url", example = "example.mp4", requiredMode = Schema.RequiredMode.REQUIRED)
    private String url;
    @Positive
    @NotNull
    @Schema(name = "subjectId", requiredMode = Schema.RequiredMode.REQUIRED)
    private Long subjectId;
    @Schema(name = "RequiresPaid")
    private boolean RequiresPaid;


}
