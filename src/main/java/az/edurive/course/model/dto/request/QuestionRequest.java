package az.edurive.course.model.dto.request;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Positive;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class QuestionRequest {
    @NotBlank(message = "text should not be empty")
    @Schema(name = "text", example = "text", requiredMode = Schema.RequiredMode.REQUIRED)
    private String text;
    @Positive
    @NotNull
    @Schema(name = "quizId", requiredMode = Schema.RequiredMode.REQUIRED)
    private Long quizId;
}
