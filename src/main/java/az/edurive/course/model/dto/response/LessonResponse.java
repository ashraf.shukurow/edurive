package az.edurive.course.model.dto.response;

import jakarta.validation.constraints.Positive;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class LessonResponse {
    private Long id;
    private String lessonName;
    private List<SubjectResponse> subjectResponse;
    private QuizResponse quizResponse;
    private boolean locked;
}
