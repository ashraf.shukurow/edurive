package az.edurive.course.model.dto.response;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class OptionResponse {
    private Long id;
//    private Long questionId;
    private String optionText;
    private boolean isCorrectOption;
}
