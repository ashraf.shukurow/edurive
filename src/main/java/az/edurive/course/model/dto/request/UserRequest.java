package az.edurive.course.model.dto.request;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.*;
import lombok.AccessLevel;
import lombok.Data;
import lombok.experimental.FieldDefaults;

@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
public class UserRequest {
    @NotBlank(message = "Name cannot be empty")
    @Schema(name = "UserName", example = "JackSon", requiredMode = Schema.RequiredMode.REQUIRED)
    String name;
    @NotBlank(message = "Surname cannot be empty")
    @Schema(name = "surname", example = "Anderson", requiredMode = Schema.RequiredMode.REQUIRED)
    String surname;
    @NotNull
    @Email(regexp = "^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\\.[a-zA-Z]{2,}$", message = "Invalid email address")
    @Schema(name = "gmail", example = "example@gmail.com", requiredMode = Schema.RequiredMode.REQUIRED)
    String gmail;
    @Size(min = 8)
    @NotBlank(message = "Password cannot be empty")
    @Schema(name = "password", example = "example123", requiredMode = Schema.RequiredMode.REQUIRED)
    String password;
    @NotNull
    @Pattern(regexp = "^(?:(70|77|50|55|99|51)(?:\\s\\d{3}){2}\\s\\d{2}|\\d{9}|(?:70|77|50|55|99|51)\\s\\d{6})$", message = "Invalid phone number format. Use 'XX XXX XX XX' or 'XXXXXXXXX' format.")
    @Schema(name = "phoneNumber", example = "707777777", requiredMode = Schema.RequiredMode.REQUIRED)
    String phoneNumber;

}
