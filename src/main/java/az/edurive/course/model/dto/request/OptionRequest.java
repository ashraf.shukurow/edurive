package az.edurive.course.model.dto.request;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Positive;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class OptionRequest {
    @Positive
    @NotNull
    @Schema(name = "questionId", example = "1", requiredMode = Schema.RequiredMode.REQUIRED)
    private Long questionId;
    @Schema(name = "optionText", example = "how many lights does have in traffic light?", requiredMode = Schema.RequiredMode.REQUIRED)
    private String optionText;
    @Schema(name = "isCorrectOption", example = "true", requiredMode = Schema.RequiredMode.REQUIRED)
    private boolean isCorrectOption;
}
