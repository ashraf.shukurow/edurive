package az.edurive.course.model.dto.response;

import lombok.Builder;
import lombok.Data;

import java.util.List;

@Data
@Builder
public class QuestionResponse {
    private Long id;
    private String text;
//    private Long quizId;
    private List<OptionResponse> optionResponses;
}
