package az.edurive.course.model.dto.request;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.Pattern;
import jakarta.validation.constraints.Size;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class UserEditRequest {
    @Schema(name = "UserName", example = "JackSon", required = true)
    private String name;

    @Schema(name = "surname", example = "Anderson", required = true)
    private String surname;

    @Schema(name = "gmail", example = "example@gmail.com", required = true)
    @Email(regexp = "^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\\.[a-zA-Z]{2,}$",
            message = "Invalid email address")
    private String gmail;

    @Schema(name = "password", example = "example123", required = true)
    @Size(min = 8, message = "Password must be at least 8 characters long")
    private String password;

    @Schema(name = "phoneNumber", example = "707777777", required = true)
    @Pattern(
            regexp = "^(?:(70|77|50|55|99|51)(?:\\s\\d{3}){2}\\s\\d{2}|\\d{9}|(?:70|77|50|55|99|51)\\s\\d{6})$",
            message = "Invalid phone number format. Use 'XX XXX XX XX' or 'XXXXXXXXX' format.")
    private String phoneNumber;
}
