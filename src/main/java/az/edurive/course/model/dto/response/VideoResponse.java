package az.edurive.course.model.dto.response;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class VideoResponse {
    private Long id;
    private String title;
    private String url;
    private boolean requiresPaid;
    @JsonIgnore
    private SubjectResponse subjectResponse;

}
