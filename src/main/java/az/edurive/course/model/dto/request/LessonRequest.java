package az.edurive.course.model.dto.request;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotBlank;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class LessonRequest {
    @NotBlank(message = "Lesson Name cannot be empty")
    @Schema(name = "lessonName", example = "Driving", requiredMode = Schema.RequiredMode.REQUIRED)
    private String lessonName;
    private boolean locked;
}
