package az.edurive.course.model.dto.response;

import az.edurive.course.exception.UnAuthorizedException;
import az.edurive.course.model.entity.User;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class SubjectResponse {
    private Long id;
    private String subjectName;
    @JsonIgnore
    private LessonResponse lessonResponse;
    private VideoResponse videoResponse;
}
