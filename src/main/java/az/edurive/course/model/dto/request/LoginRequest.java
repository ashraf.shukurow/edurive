package az.edurive.course.model.dto.request;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import org.springframework.stereotype.Component;

@Data
@Component
public class LoginRequest {
    @Schema(name = "gmail", example = "example@gmail.com", requiredMode = Schema.RequiredMode.REQUIRED)
    private String gmail;
    @Schema(name = "password", example = "Example123", requiredMode = Schema.RequiredMode.REQUIRED)
    private String password;
}
