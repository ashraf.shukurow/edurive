package az.edurive.course.model.dto.request;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Positive;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class QuizRequest {
    @NotBlank(message = "Quiz Name cannot be empty")
    @Schema(name = "quizName", example = "Quiz1", requiredMode = Schema.RequiredMode.REQUIRED)
    private String quizName;
    @Positive
    @NotNull
    @Schema(name = "lessonId", requiredMode = Schema.RequiredMode.REQUIRED)
    private Long lessonId;
}
