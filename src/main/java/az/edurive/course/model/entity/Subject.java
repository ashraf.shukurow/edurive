package az.edurive.course.model.entity;

import jakarta.persistence.*;
import lombok.*;

@Setter
@Getter
@Table(name = "subjects")
@Entity
@NoArgsConstructor
@AllArgsConstructor
public class Subject {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String subjectName;

    @ManyToOne(cascade = CascadeType.MERGE,fetch = FetchType.EAGER)
    @JoinColumn(name = "lesson_id",referencedColumnName = "id")
    private Lesson lesson;

    @OneToOne(mappedBy = "subject",cascade = CascadeType.ALL)
    private Video video;


}
