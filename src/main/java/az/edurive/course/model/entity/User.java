package az.edurive.course.model.entity;

import az.edurive.course.model.enums.RoleType;
import jakarta.persistence.*;
import lombok.*;
import lombok.experimental.FieldDefaults;

import java.util.List;
import java.util.Set;

@Getter
@Setter
@Table(name = "users")
@Entity
@NoArgsConstructor
@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;
    @Column(name = "user_name", length = 25, nullable = false)
    String name;
    @Column(name = "user_surname", length = 35, nullable = false)
    String surname;
    @Column(name = "user_gmail", nullable = false)
    String gmail;
    @Column(nullable = false)
    String password;
    @Enumerated(value = EnumType.STRING)
    @Column(name = "role_type", nullable = false)
    RoleType roleType;
    @Column(name = "phone_number")
    String phoneNumber;

    @Column(name = "is_paid")
    private boolean isPaid;
    @Column(name = "is_active")
    private boolean isActive;

    @ManyToMany
    @JoinTable(
            name = "user_accessed_lessons",
            joinColumns = @JoinColumn(name = "user_id"),
            inverseJoinColumns = @JoinColumn(name = "lesson_id")
    )
    private Set<Lesson> accessedLessons;

    public void addAccessedVideo(Lesson lesson) {
        accessedLessons.add(lesson);
        lesson.getAccessedByLessons().add(this);

    }


}
