package az.edurive.course.model.entity;

import jakarta.persistence.*;
import lombok.*;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Setter
@Getter
@Table(name = "lessons")
@Entity
@NoArgsConstructor
@AllArgsConstructor
public class Lesson {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String lessonName;
    @OneToMany(mappedBy = "lesson", cascade = CascadeType.ALL)
    private List<Subject> subjects;

    @OneToOne(mappedBy = "lesson", cascade = CascadeType.ALL)
    private Quiz quiz;

    @ManyToMany(mappedBy = "accessedLessons")
    private Set<User> accessedByLessons;


    public Set<User> getAccessedByUsers() {
        if (accessedByLessons == null) {
            accessedByLessons = new HashSet<>();
        }
        return accessedByLessons;
    }

}
