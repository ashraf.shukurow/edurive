package az.edurive.course.model.entity;

import jakarta.persistence.*;
import lombok.*;

@Entity
@Setter
@Getter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "options")
public class Option {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String optionText;
    private boolean isCorrectOption;

    @ManyToOne(cascade = CascadeType.MERGE)
    @JoinColumn(name = "question_id", referencedColumnName = "id")
    Question question;


}
