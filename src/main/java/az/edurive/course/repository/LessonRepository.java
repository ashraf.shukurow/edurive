package az.edurive.course.repository;

import az.edurive.course.model.entity.Lesson;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface LessonRepository extends JpaRepository<Lesson,Long> {
    boolean existsByLessonName(String name);
}
