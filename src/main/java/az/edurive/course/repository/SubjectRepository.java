package az.edurive.course.repository;

import az.edurive.course.model.entity.Subject;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface SubjectRepository extends JpaRepository<Subject,Long> {
    boolean existsBySubjectName(String name);
    @Query("select s from Subject s WHERE s.lesson.id= :lessonId ")
    List<Subject> findSubjectsByLessonId(Long lessonId);
}
