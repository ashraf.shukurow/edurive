package az.edurive.course.service;

import az.edurive.course.model.dto.request.SubjectRequest;
import az.edurive.course.model.dto.response.SubjectResponse;
import az.edurive.course.model.dto.response.VideoResponse;
import az.edurive.course.model.entity.Subject;

import java.util.List;

public interface SubjectService {

    void addSubject(SubjectRequest subjectRequest);

    SubjectResponse getSubjectById(Long id);

    List<SubjectResponse> getAllSubject();

    void updateSubject(SubjectRequest subjectRequest, Long id);

    List<SubjectResponse> getAllSubjectByLessonId(Long lessonId);

    void deleteSubjectById(Long id);

    List<VideoResponse> getAllVideoBySubjectId(Long subjectId);
}
