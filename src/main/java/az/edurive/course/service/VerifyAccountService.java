package az.edurive.course.service;

public interface VerifyAccountService {
    void verify(String token);
    boolean isActivated(String gmail);
}
