package az.edurive.course.service;

import az.edurive.course.model.dto.request.VideoRequest;
import az.edurive.course.model.dto.response.VideoResponse;
import az.edurive.course.model.entity.Video;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;

public interface VideoService {

    void createVideo(VideoRequest videoRequest, MultipartFile multipartFile) throws IOException;

    VideoResponse getVideoById(Long id,Long userId);

    List<VideoResponse> getAllVideo();

    void deleteVideoById(Long id);


}
