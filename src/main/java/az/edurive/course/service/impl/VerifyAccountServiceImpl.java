package az.edurive.course.service.impl;

import az.edurive.course.exception.NotFoundException;
import az.edurive.course.repository.UserRepository;
import az.edurive.course.security.JwtTokenProvider;
import az.edurive.course.service.VerifyAccountService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
@Slf4j
public class VerifyAccountServiceImpl implements VerifyAccountService {
    private final JwtTokenProvider jwtTokenProvider;
    private final UserRepository userRepository;
    @Override
    public void verify(String token) {
        jwtTokenProvider.validateToken(token);
        var email=jwtTokenProvider.getGmailFromJwt(token);
        var user=userRepository.findByGmail(email).orElseThrow(
                ()->new NotFoundException("User Not Found With gmail: "+email)
        );
        user.setActive(true);
        userRepository.save(user);

    }

    @Override
    public boolean isActivated(String gmail) {
        var user=userRepository.findByGmailAndActiveIsTrue(gmail);
        return user.isPresent();
    }
}
