package az.edurive.course.service.impl;

import az.edurive.course.exception.NotFoundException;
import az.edurive.course.mapper.QuizMapper;
import az.edurive.course.model.dto.request.QuizRequest;
import az.edurive.course.model.dto.response.QuizResponse;
import az.edurive.course.model.entity.Quiz;
import az.edurive.course.repository.QuizRepository;
import az.edurive.course.service.QuizService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class QuizServiceImpl implements QuizService {
    private final QuizRepository quizRepository;
    private final QuizMapper quizMapper;

    @Override
    public void createQuiz(QuizRequest quizRequest) {
        quizRepository.save(quizMapper.requestToEntity(quizRequest));
    }

    @Override
    public QuizResponse getQuizById(Long id) {
        Quiz quiz = quizRepository.findById(id).orElseThrow(() -> new NotFoundException("Quiz not found with id: " + id));
        return quizMapper.entityToResponse(quiz);
    }

    @Override
    public List<QuizResponse> getAllQuiz() {
        return quizRepository.findAll().stream().map(quizMapper::entityToResponse).toList();
    }

    @Override
    public void updateQuiz(QuizRequest quizRequest, Long id) {
        Quiz quiz = quizRepository.findById(id).orElseThrow(() -> new NotFoundException("Quiz not found with id: " + id));
        Quiz updatedQuiz = quizMapper.requestToEntity(quizRequest);
        updatedQuiz.setId(quiz.getId());
        updatedQuiz.setLesson(quiz.getLesson());
        quizRepository.save(updatedQuiz);
    }

    @Override
    public void deleteQuiz(Long id) {
        Quiz quiz = quizRepository.findById(id).orElseThrow(() -> new NotFoundException("Quiz not found with id: " + id));
        quizRepository.delete(quiz);
    }
}
