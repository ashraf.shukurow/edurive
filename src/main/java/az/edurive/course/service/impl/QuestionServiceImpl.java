package az.edurive.course.service.impl;

import az.edurive.course.exception.NotFoundException;
import az.edurive.course.mapper.QuestionMapper;
import az.edurive.course.model.dto.request.QuestionRequest;
import az.edurive.course.model.dto.response.QuestionResponse;
import az.edurive.course.model.entity.Question;
import az.edurive.course.model.entity.Quiz;
import az.edurive.course.repository.QuestionRepository;
import az.edurive.course.repository.QuizRepository;
import az.edurive.course.service.QuestionService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class QuestionServiceImpl implements QuestionService {
    private final QuestionRepository questionRepository;
    private final QuestionMapper questionMapper;
    private final QuizRepository quizRepository;

    @Override
    public void createQuestion(QuestionRequest questionRequest) {
        Question question = questionMapper.requestToEntity(questionRequest);
        quizRepository.findById(questionRequest.getQuizId()).orElseThrow(() -> new NotFoundException("Quiz not found with id: " + questionRequest.getQuizId()));
        questionRepository.save(question);
    }

    @Override
    public QuestionResponse getQuestionById(Long id) {
        Question question = questionRepository.findById(id).orElseThrow(() -> new NotFoundException("There is no question this id: " + id));
        return questionMapper.entityToResponse(question);
    }

    @Override
    public List<QuestionResponse> getAllQuestion() {
        return questionRepository.findAll()
                .stream()
                .map(questionMapper::entityToResponse)
                .toList();
    }

    @Override
    public void updateQuestion(QuestionRequest questionRequest, Long id) {
        Question question = questionRepository.findById(id).orElseThrow(() -> new NotFoundException("There is no question this id: " + id));
        Question updatedQuestion = questionMapper.requestToEntity(questionRequest);
        updatedQuestion.setId(question.getId());
        updatedQuestion.setQuiz(question.getQuiz());
        questionRepository.save(updatedQuestion);
    }

    @Override
    public void deleteQuestion(Long id) {
        Question question = questionRepository.findById(id).orElseThrow(() -> new NotFoundException("There is no question this id: " + id));
        questionRepository.delete(question);
    }
}
