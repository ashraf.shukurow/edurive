package az.edurive.course.service.impl;

import az.edurive.course.exception.AlreadyExistsException;
import az.edurive.course.exception.IncorrectPasswordException;
import az.edurive.course.exception.NotFoundException;
import az.edurive.course.exception.UserNotActiveException;
import az.edurive.course.mapper.UserMapper;
import az.edurive.course.model.dto.request.LoginRequest;
import az.edurive.course.model.dto.request.UserRequest;
import az.edurive.course.model.dto.response.AuthResponse;
import az.edurive.course.model.dto.response.TokenResponse;
import az.edurive.course.model.dto.response.UserResponse;
import az.edurive.course.model.entity.User;
import az.edurive.course.model.enums.RoleType;
import az.edurive.course.model.enums.TokenType;
import az.edurive.course.repository.UserRepository;
import az.edurive.course.security.JwtTokenProvider;
import az.edurive.course.security.PasswordEncoder;
import az.edurive.course.security.UserPrincipal.UserPrincipal;
import az.edurive.course.service.AuthService;
import az.edurive.course.service.EmailService;
import jakarta.mail.MessagingException;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.InternalAuthenticationServiceException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

@Slf4j
@Service
@RequiredArgsConstructor
public class AuthServiceImpl implements AuthService {

    private final UserRepository userRepository;
    private final UserMapper userMapper;
    private final PasswordEncoder passwordEncoder;
    private final AuthenticationManager authenticationManager;
    private final JwtTokenProvider jwtTokenProvider;
    private final EmailService emailService;

    @Override
    public AuthResponse login(LoginRequest loginRequest) {
        log.info("Attempting to log in with email: {}", loginRequest.getGmail());

        User user = userRepository.findByGmail(loginRequest.getGmail())
                .orElseThrow(() -> new NotFoundException("User not found with email: " + loginRequest.getGmail()));

        if (!user.isActive()) {
            throw new UserNotActiveException("User is not active.");
        }
        try {
            Authentication authentication = authenticationManager.authenticate(
                    new UsernamePasswordAuthenticationToken(loginRequest.getGmail(), loginRequest.getPassword())
            );
            SecurityContextHolder.getContext().setAuthentication(authentication);

            TokenResponse tokenResponse = new TokenResponse();
            tokenResponse.setAccessToken(jwtTokenProvider.generateToken((UserDetails) authentication.getPrincipal(), TokenType.ACCESS_TOKEN));
            tokenResponse.setRefreshToken(jwtTokenProvider.generateToken((UserDetails) authentication.getPrincipal(), TokenType.REFRESH_TOKEN));

            return AuthResponse.builder()
                    .tokenResponse(tokenResponse)
                    .userResponse(userMapper.entityToResponse(user))
                    .build();

        } catch (BadCredentialsException e) {
            log.error("Incorrect password for user with email: {}", loginRequest.getGmail());
            throw new IncorrectPasswordException("Incorrect password provided.");
        } catch (InternalAuthenticationServiceException e) {
            log.error("User not found.");
            throw new NotFoundException("User not found with email: " + loginRequest.getGmail());
        }
    }

    @Override
    public TokenResponse refreshToken(UserDetails userDetails) {
        TokenResponse tokenResponse = new TokenResponse();
        tokenResponse.setAccessToken(jwtTokenProvider.generateToken(userDetails, TokenType.ACCESS_TOKEN));
        tokenResponse.setRefreshToken(jwtTokenProvider.generateToken(userDetails, TokenType.REFRESH_TOKEN));
        return tokenResponse;
    }

    @Override
    public UserResponse registration(UserRequest userRequest) {
        log.info("Registering user with email: {}", userRequest.getGmail());

        if (userRepository.existsByGmail(userRequest.getGmail())) {
            throw new AlreadyExistsException("A user with this email already exists: " + userRequest.getGmail());
        }

        User user = userMapper.requestToEntity(userRequest);
        user.setPassword(passwordEncoder.passwordEncode(userRequest.getPassword()));
        user.setRoleType(RoleType.User);
        userRepository.save(user);

        UserPrincipal userDetails = new UserPrincipal();
        userDetails.setGmail(user.getGmail());
        userDetails.setPassword(user.getPassword());
        String token = jwtTokenProvider.generateToken(userDetails, TokenType.ACCESS_TOKEN);

        try {
            log.info("Sending email verification.");
            emailService.sendMailToAdmin(userRequest);
            emailService.sendMail(user.getGmail(), token);
        } catch (MessagingException e) {
            log.error("Failed to send verification email.");
            throw new RuntimeException("Unable to send verification email. Please try again later.");
        }

        return userMapper.entityToResponse(user);
    }

    @Override
    public String forgotPassword(String email) {
        log.info("Processing forgot password request for email: {}", email);

        User user = userRepository.findByGmail(email)
                .orElseThrow(() -> new NotFoundException("User not found with this email: " + email));

        try {
            log.info("Sending password reset email.");
            emailService.sendSetPasswordToEmail(user.getGmail());
            return "Please check your email to set a new password.";
        } catch (MessagingException e) {
            log.error("Failed to send password reset email.");
            throw new RuntimeException("Unable to send password reset email. Please try again.");
        }
    }

    @Override
    public String setPassword(String email, String newPassword) {
        log.info("Setting new password for email: {}", email);

        User user = userRepository.findByGmail(email)
                .orElseThrow(() -> new NotFoundException("User not found with this email: " + email));

        user.setPassword(passwordEncoder.passwordEncode(newPassword));
        userRepository.save(user);

        return "New password has been set successfully. Please log in with the new password.";
    }
}
