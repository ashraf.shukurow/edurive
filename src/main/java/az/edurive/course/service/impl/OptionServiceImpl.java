package az.edurive.course.service.impl;

import az.edurive.course.exception.NotFoundException;
import az.edurive.course.mapper.OptionMapper;
import az.edurive.course.model.dto.request.OptionRequest;
import az.edurive.course.model.dto.response.OptionResponse;
import az.edurive.course.model.entity.Option;
import az.edurive.course.model.entity.Question;
import az.edurive.course.repository.OptionRepository;
import az.edurive.course.repository.QuestionRepository;
import az.edurive.course.service.OptionService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class OptionServiceImpl implements OptionService {
    private final OptionRepository optionRepository;
    private final OptionMapper optionMapper;
    private final QuestionRepository questionRepository;

    @Override
    public void createOption(OptionRequest optionRequest) {
        Option option = optionMapper.requestToEntity(optionRequest);
        questionRepository.findById(optionRequest.getQuestionId()).orElseThrow(() -> new NotFoundException("There is no question this id: " + optionRequest.getQuestionId()));
        optionRepository.save(option);
    }

    @Override
    public OptionResponse getOptionById(Long id) {
        Option option = optionRepository.findById(id).orElseThrow(() -> new NotFoundException("there is no option this id: " + id));
        return optionMapper.entityToResponse(option);
    }

    @Override
    public List<OptionResponse> getAllOption() {
        return optionRepository.findAll()
                .stream()
                .map(optionMapper::entityToResponse)
                .toList();
    }

    @Override
    public void updateOption(OptionRequest optionRequest, Long id) {
        Option option = optionRepository.findById(id).orElseThrow(() -> new NotFoundException("there is no option this id: " + id));
        Option updatedOption = optionMapper.requestToEntity(optionRequest);
        updatedOption.setId(option.getId());
        updatedOption.setCorrectOption(optionRequest.isCorrectOption());
        updatedOption.setQuestion(option.getQuestion());
        updatedOption.setOptionText(optionRequest.getOptionText());
        optionRepository.save(updatedOption);
    }

    @Override
    public void deleteOption(Long id) {
        Option option = optionRepository.findById(id).orElseThrow(() -> new NotFoundException("there is no option this id: " + id));
        optionRepository.delete(option);
    }
}
