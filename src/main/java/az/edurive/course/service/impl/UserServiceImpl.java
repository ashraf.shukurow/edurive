package az.edurive.course.service.impl;

import az.edurive.course.exception.AlreadyExistsException;
import az.edurive.course.exception.NotFoundException;
import az.edurive.course.mapper.UserMapper;
import az.edurive.course.mapper.VideoMapper;
import az.edurive.course.model.dto.request.UserEditRequest;
import az.edurive.course.model.dto.request.UserRequest;
import az.edurive.course.model.dto.response.UserResponse;
import az.edurive.course.model.dto.response.VideoResponse;
import az.edurive.course.model.entity.User;
import az.edurive.course.model.entity.Video;
import az.edurive.course.repository.UserRepository;
import az.edurive.course.security.PasswordEncoder;
import az.edurive.course.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Set;

/**
 * @author ashraf on 31.01.24
 * @project course
 */
@Service
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {
    private final UserRepository userRepository;
    private final UserMapper userMapper;
    private final VideoMapper videoMapper;
    private final PasswordEncoder passwordEncoder;

    @Override
    public void createUser(UserRequest userRequest) {
        User user = userMapper.requestToEntity(userRequest);
        if (userRepository.existsByGmail(user.getGmail())) {
            throw new AlreadyExistsException("There is a user with this gmail:" + user.getGmail());
        }
        userRepository.save(user);
    }

    @Override
    public List<UserResponse> getAllUser() {
        List<User> list = userRepository.findAll();
        return list.stream().map(userMapper::entityToResponse).toList();
    }

    @Override
    public void deleteUserById(Long id) {
        User user = userRepository.findById(id).orElseThrow(() -> new NotFoundException("There is no user with this id=" + id));
        userRepository.delete(user);
    }

    @Override
    public UserResponse getUserById(Long id) {
        User user = userRepository.findById(id).orElseThrow(() -> new NotFoundException("There is no user with this id=" + id));
        return userMapper.entityToResponse(user);
    }

    @Override
    public void updateUser(UserEditRequest userEditRequest, Long id) {
        User user = userRepository.findById(id).orElseThrow(() -> new NotFoundException("There is no user with this id=" + id));
        if (userEditRequest.getPassword() != null) {
            user.setPassword(this.passwordEncoder.passwordEncode(userEditRequest.getPassword()));
        } else {
            user.setPassword(user.getPassword());
        }

        if (userEditRequest.getName() != null) {
            user.setName(userEditRequest.getName());
        } else {
            user.setName(user.getName());
        }

        if (userEditRequest.getSurname() != null) {
            user.setSurname(userEditRequest.getSurname());
        } else {
            user.setSurname(user.getSurname());
        }

        if (userEditRequest.getGmail() != null) {
            user.setGmail(userEditRequest.getGmail());
        } else {
            user.setGmail(user.getGmail());
        }

        if (userEditRequest.getPassword() != null) {
            user.setPhoneNumber(userEditRequest.getPhoneNumber());
        } else {
            user.setPhoneNumber(user.getPhoneNumber());
        }
        userRepository.save(user);

    }


}
