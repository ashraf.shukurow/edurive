package az.edurive.course.service.impl;

import az.edurive.course.exception.AlreadyExistsException;
import az.edurive.course.exception.NotFoundException;
import az.edurive.course.mapper.LessonMapper;
import az.edurive.course.mapper.SubjectMapper;
import az.edurive.course.model.dto.request.LessonRequest;
import az.edurive.course.model.dto.response.LessonResponse;
import az.edurive.course.model.dto.response.SubjectResponse;
import az.edurive.course.model.dto.response.VideoResponse;
import az.edurive.course.model.entity.Lesson;
import az.edurive.course.model.entity.Subject;
import az.edurive.course.model.entity.User;
import az.edurive.course.repository.LessonRepository;
import az.edurive.course.repository.SubjectRepository;
import az.edurive.course.repository.UserRepository;
import az.edurive.course.service.LessonService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Set;

@Service
@RequiredArgsConstructor
public class LessonServiceImpl implements LessonService {
    private final LessonRepository lessonRepository;
    private final LessonMapper lessonMapper;
    private final UserRepository userRepository;


    @Override
    public void createLesson(LessonRequest lessonRequest) {
        Lesson lesson = lessonMapper.requestToEntity(lessonRequest);
        if (lessonRepository.existsByLessonName(lesson.getLessonName())) {
            throw new AlreadyExistsException("Lesson name is already saved in db with name: " + lesson.getLessonName());
        }
        lessonRepository.save(lesson);

    }

    @Override
    public void deleteLesson(Long id) {
        Lesson lesson = lessonRepository.findById(id).orElseThrow(() -> new NotFoundException("Lesson is not found with id: " + id));
        lessonRepository.delete(lesson);
    }

    @Override
    public LessonResponse getLessonById(Long id) {
        Lesson lesson = lessonRepository.findById(id).orElseThrow(() -> new NotFoundException("Lesson is not found with id: " + id));
        return lessonMapper.entityToResponse(lesson);
    }

    @Override
    public List<LessonResponse> getAllLesson() {
        List<Lesson> lessons = lessonRepository.findAll();

        return lessons.stream().map(lessonMapper::entityToResponse).toList();
    }

    @Override
    public void updateLesson(LessonRequest lessonRequest, Long id) {
        Lesson lesson = lessonRepository.findById(id).orElseThrow(() -> new NotFoundException("Lesson is not found with id: " + id));
        Lesson updatedLesson = lessonMapper.requestToEntity(lessonRequest);
        updatedLesson.setId(lesson.getId());
        lessonRepository.save(updatedLesson);
    }

    @Override
    public List<LessonResponse> getAllLessonByUserId(Long userId) {
        User user = userRepository.findById(userId).orElseThrow(() -> new NotFoundException("User is not found with id: " + userId));
        Set<Lesson> lessons=user.getAccessedLessons();
        return lessons.stream().map(lessonMapper::entityToResponse).toList();
    }


}
