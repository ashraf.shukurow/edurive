package az.edurive.course.service.impl;

import az.edurive.course.exception.InvalidAccessException;
import az.edurive.course.exception.NotFoundException;
import az.edurive.course.exception.UnAuthorizedException;
import az.edurive.course.model.entity.Lesson;
import az.edurive.course.model.entity.Subject;
import az.edurive.course.model.entity.User;
import az.edurive.course.model.entity.Video;
import az.edurive.course.repository.LessonRepository;
import az.edurive.course.repository.UserRepository;
import az.edurive.course.repository.VideoRepository;
import az.edurive.course.service.AccessService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;

@Service
@Slf4j
@RequiredArgsConstructor
public class AccessServiceImpl implements AccessService {
    private final UserRepository userRepository;
    private final LessonRepository lessonRepository;

    @Override
    public String giveAccess(Long userId) {
        User user = userRepository.findById(userId)
                .orElseThrow(() -> new NotFoundException("User not found with id: " + userId));
        List<Lesson> allLessons = lessonRepository.findAll();

        user.getAccessedLessons().addAll(allLessons);
        user.setPaid(true);
        userRepository.save(user);

        return "Access granted to all lessons for the user.";
    }

    @Override
    public String revokeAccess(Long userId) {
        User user = userRepository.findById(userId)
                .orElseThrow(() -> new NotFoundException("User not found with id: " + userId));
        user.getAccessedLessons().clear();
        user.setPaid(false);
        userRepository.save(user);
        return "Access revoked for the user.";
    }

}
