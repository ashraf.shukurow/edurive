package az.edurive.course.service.impl;

import az.edurive.course.exception.NotFoundException;
import az.edurive.course.exception.UnAuthorizedException;
import az.edurive.course.mapper.VideoMapper;
import az.edurive.course.model.dto.request.VideoRequest;
import az.edurive.course.model.dto.response.VideoResponse;
import az.edurive.course.model.entity.Subject;
import az.edurive.course.model.entity.Video;
import az.edurive.course.repository.SubjectRepository;
import az.edurive.course.repository.UserRepository;
import az.edurive.course.repository.VideoRepository;
import az.edurive.course.service.VideoService;
import az.edurive.course.util.FileUtil;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import az.edurive.course.model.entity.User;

import java.io.IOException;
import java.nio.file.Path;
import java.util.List;

@Service
@RequiredArgsConstructor
public class VideoServiceImpl implements VideoService {
    private final SubjectRepository subjectRepository;
    private final VideoRepository videoRepository;
    private final FileUtil fileUtil;
    private final VideoMapper videoMapper;
    private final UserRepository userRepository;

    @Override
    public void createVideo(VideoRequest videoRequest, MultipartFile videoFile) throws IOException {
        final String bucketName = "library.management.system";
        if (videoRequest.getSubjectId() != null && !subjectRepository.existsById(videoRequest.getSubjectId())) {
            throw new NotFoundException("Subject not found with id: " + videoRequest.getSubjectId());
        }
        Video video = videoMapper.requestToEntity(videoRequest);
        if (videoRequest.getSubjectId() != null) {
            Subject subject = subjectRepository.
                    findById(videoRequest.getSubjectId())
                    .orElseThrow(() -> new NotFoundException("Subject not found with id: " + videoRequest.getSubjectId()));
            video.setSubject(subject);
        }
        Video savedVideo = videoRepository.save(video);
        if (videoFile != null) {
            String region = "eu-central-1";
            String objectKey = "images/" + videoFile.getOriginalFilename();
            String s3ObjectUrl = "https://s3." + region + ".amazonaws.com/" + bucketName + "/" + objectKey;
            savedVideo.setUrl(s3ObjectUrl);
            fileUtil.uploadFile(bucketName, objectKey, videoFile);
            videoRepository.save(savedVideo);
        }

    }

    @Override
    public VideoResponse getVideoById(Long id, Long userId) {
        Video video = videoRepository.findById(id)
                .orElseThrow(() -> new NotFoundException("No video found with this id: " + id));

        User user = userRepository.findById(userId).orElseThrow(() -> new NotFoundException("No User found with this id: " + userId));

        if (video.isRequiresPaid() && !user.isPaid()) {
            throw new UnAuthorizedException("User has not paid for access to this video");
        }
        return videoMapper.entityToResponse(video);
    }

    @Override
    public List<VideoResponse> getAllVideo() {
        List<Video> videos = videoRepository.findAll();
        return videos.stream().map(videoMapper::entityToResponse).toList();
    }

    @Override
    public void deleteVideoById(Long id) {
        Video video = videoRepository.findById(id)
                .orElseThrow(() -> new NotFoundException("no found video with this id: " + id));
        videoRepository.delete(video);
    }
}
