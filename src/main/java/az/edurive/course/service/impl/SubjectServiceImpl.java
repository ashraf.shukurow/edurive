package az.edurive.course.service.impl;

import az.edurive.course.exception.AlreadyExistsException;
import az.edurive.course.exception.NotFoundException;
import az.edurive.course.mapper.LessonMapper;
import az.edurive.course.mapper.SubjectMapper;
import az.edurive.course.model.dto.request.SubjectRequest;
import az.edurive.course.model.dto.response.SubjectResponse;
import az.edurive.course.model.dto.response.VideoResponse;
import az.edurive.course.model.entity.Lesson;
import az.edurive.course.model.entity.Subject;
import az.edurive.course.repository.LessonRepository;
import az.edurive.course.repository.SubjectRepository;
import az.edurive.course.service.SubjectService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class SubjectServiceImpl implements SubjectService {
    private final SubjectRepository subjectRepository;
    private final SubjectMapper subjectMapper;
    private final LessonRepository lessonRepository;

    @Override
    public void addSubject(SubjectRequest subjectRequest) {
        Subject subject = subjectMapper.requestToEntity(subjectRequest);
        if (subjectRepository.existsBySubjectName(subject.getSubjectName())) {
            throw new AlreadyExistsException("subject is already exists with name: " + subject.getSubjectName());
        }else{
            lessonRepository.findById(subjectRequest.getLessonId()).orElseThrow(() -> new NotFoundException("Lesson is not found with id: " + subjectRequest.getLessonId()));
        }
        subjectRepository.save(subject);
    }

    @Override
    public SubjectResponse getSubjectById(Long id) {
        Subject subject = subjectRepository
                .findById(id).orElseThrow(() -> new NotFoundException("subject is not found with id: " + id));
        return subjectMapper.entityToResponse(subject);
    }

    @Override
    public List<SubjectResponse> getAllSubject() {
        List<Subject> subjects = subjectRepository.findAll();
        return subjects.stream().map(subjectMapper::entityToResponse).toList();
    }

    @Override
    public void updateSubject(SubjectRequest subjectRequest, Long id) {
        Subject subject = subjectRepository
                .findById(id).orElseThrow(() -> new NotFoundException("subject is not found with id: " + id));
        Subject updatedSubject = subjectMapper.requestToEntity(subjectRequest);
        updatedSubject.setId(subject.getId());
        updatedSubject.setLesson(subject.getLesson());
        subjectRepository.save(updatedSubject);

    }

    @Override
    public List<SubjectResponse> getAllSubjectByLessonId(Long lessonId) {
        List<Subject> subjects = subjectRepository.findSubjectsByLessonId(lessonId);
        return subjects.stream().map(subjectMapper::entityToResponse).toList();
    }

    @Override
    public void deleteSubjectById(Long id) {
        Subject subject = subjectRepository
                .findById(id).orElseThrow(() -> new NotFoundException("subject is not found with id: " + id));
        subjectRepository.delete(subject);
    }

    @Override
    public List<VideoResponse> getAllVideoBySubjectId(Long subjectId) {
        return null;
    }
}
