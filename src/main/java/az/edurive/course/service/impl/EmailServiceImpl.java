package az.edurive.course.service.impl;

import az.edurive.course.exception.NotFoundException;
import az.edurive.course.model.dto.request.UserRequest;
import az.edurive.course.model.entity.User;
import az.edurive.course.repository.UserRepository;
import az.edurive.course.service.EmailService;
import jakarta.mail.MessagingException;
import jakarta.mail.internet.MimeMessage;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMailMessage;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class EmailServiceImpl implements EmailService {
    private static final String Email_Verification = """
            <div><p>Email Verification 📧:</p>
            <p>Dear User, %s 🥰,</p>
            <p>Please click the link below to verify your email address 😍</p>
            <p><img src="https://edurive.s3.eu-central-1.amazonaws.com/email-verification" width="300" height="200"></p>
            <p><a href="https://edurive.az/v1/verify/%s" target="_blank">Verify Email Address</a></p></div>
            """;

    private static final String Password_Change = """
            <div>
            <p>Please click the link below to change your password:</p>
            <p><img src="https://edurive.s3.eu-central-1.amazonaws.com/password_change" width="300" height="200"></p>
            <a href="https://edurive.az/ChangePassword?email=%s" target="_blank">Click here to change your password</a>
            </div>
            """;

    private final JavaMailSender javaMailSender;
    @Value("${spring.mail.username}")
    private String adminEmail;
    private final UserRepository userRepository;

    @Override
    public void sendMailToAdmin(UserRequest userRequest) {
        SimpleMailMessage message = new SimpleMailMessage();

        message.setTo("cinara.mustafayeva.cm@gmail.com");
        message.setFrom(adminEmail);
        message.setSubject("New User Registration");
        message.setText("The user " + userRequest.getName() + " has successfully registered And User's info: " + "\n" +
                "name: " + userRequest.getName() + "\n" + "surname: " + userRequest.getSurname() + "\n" +
                "gmail: " + userRequest.getGmail() + "\n" + "phoneNumber: " + userRequest.getPhoneNumber());

        javaMailSender.send(message);

    }
    @Override
    public void sendMail(String to, String token) throws MessagingException {
        MimeMessage message = this.javaMailSender.createMimeMessage();
        MimeMessageHelper mimeMessageHelper = new MimeMessageHelper(message);
        mimeMessageHelper.setTo(to);
        User user = userRepository.findByGmail(to).orElseThrow(() -> {
            return new NotFoundException("Not found User with this email: " + to);
        });
        String username = user.getName();
        mimeMessageHelper.setSubject("Email Verification");
        mimeMessageHelper.setText(Email_Verification.formatted(username, token), true);
        javaMailSender.send(message);
    }
    @Override
    public void sendSetPasswordToEmail(String email) throws MessagingException {
        MimeMessage message=javaMailSender.createMimeMessage();
        MimeMessageHelper mimeMessageHelper=new MimeMessageHelper(message);
        mimeMessageHelper.setTo(email);
        mimeMessageHelper.setSubject("Change Password");
        mimeMessageHelper.setText(Password_Change.formatted(email),true);
        javaMailSender.send(message);
    }


}
