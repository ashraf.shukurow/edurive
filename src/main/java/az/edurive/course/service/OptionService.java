package az.edurive.course.service;

import az.edurive.course.model.dto.request.OptionRequest;
import az.edurive.course.model.dto.response.OptionResponse;

import java.util.List;

public interface OptionService {
    void createOption(OptionRequest optionRequest);

    OptionResponse getOptionById(Long id);

    List<OptionResponse> getAllOption();

    void updateOption(OptionRequest optionRequest, Long id);

    void deleteOption(Long id);

}
