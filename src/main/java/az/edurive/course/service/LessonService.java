package az.edurive.course.service;

import az.edurive.course.model.dto.request.LessonRequest;
import az.edurive.course.model.dto.response.LessonResponse;
import az.edurive.course.model.dto.response.SubjectResponse;
import az.edurive.course.model.dto.response.VideoResponse;
import org.springframework.http.ResponseEntity;

import java.util.List;

public interface LessonService {
    void createLesson(LessonRequest lessonRequest);

    void deleteLesson(Long id);

    LessonResponse getLessonById(Long id);

    List<LessonResponse> getAllLesson();

    void updateLesson(LessonRequest lessonRequest, Long id);

    List<LessonResponse> getAllLessonByUserId(Long userId);
}