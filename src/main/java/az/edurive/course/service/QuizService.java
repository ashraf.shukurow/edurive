package az.edurive.course.service;

import az.edurive.course.model.dto.request.QuizRequest;
import az.edurive.course.model.dto.response.QuizResponse;

import java.util.List;

public interface QuizService {
    void createQuiz(QuizRequest quizRequest);

    QuizResponse getQuizById(Long id);

    List<QuizResponse> getAllQuiz();

    void updateQuiz(QuizRequest quizRequest, Long id);

    void deleteQuiz(Long id);
}
