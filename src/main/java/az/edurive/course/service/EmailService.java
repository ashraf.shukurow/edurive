package az.edurive.course.service;

import az.edurive.course.model.dto.request.UserRequest;
import jakarta.mail.MessagingException;

public interface EmailService {
     void sendMailToAdmin(UserRequest userRequest);
     void sendMail(String to, String token) throws MessagingException;
     void sendSetPasswordToEmail(String email) throws MessagingException;
}
