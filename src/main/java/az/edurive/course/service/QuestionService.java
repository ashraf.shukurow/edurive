package az.edurive.course.service;

import az.edurive.course.model.dto.request.QuestionRequest;
import az.edurive.course.model.dto.response.QuestionResponse;
import az.edurive.course.model.entity.Question;

import java.util.List;

public interface QuestionService {
    void createQuestion(QuestionRequest questionRequest);

    QuestionResponse getQuestionById(Long id);

    List<QuestionResponse> getAllQuestion();

    void updateQuestion(QuestionRequest questionRequest, Long id);

    void deleteQuestion(Long id);
}
