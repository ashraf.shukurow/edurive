package az.edurive.course.service;

public interface AccessService {
    String giveAccess(Long userId);
    String revokeAccess(Long userId);
}
