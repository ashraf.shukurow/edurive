package az.edurive.course.exception;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.springframework.http.HttpStatus;

@EqualsAndHashCode(callSuper = true)
@AllArgsConstructor
@Data
public class InvalidAccessException extends RuntimeException {
    private final HttpStatus status=HttpStatus.BAD_REQUEST;
    private final String message;
}
