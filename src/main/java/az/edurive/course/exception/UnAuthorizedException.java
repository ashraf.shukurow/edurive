package az.edurive.course.exception;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.springframework.http.HttpStatus;

@EqualsAndHashCode(callSuper = true)
@AllArgsConstructor
@Data
@NoArgsConstructor
public class UnAuthorizedException extends RuntimeException{
    private final HttpStatus httpStatus=HttpStatus.UNAUTHORIZED;
    private  String message;

}
