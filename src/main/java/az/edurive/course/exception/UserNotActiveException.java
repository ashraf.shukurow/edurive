package az.edurive.course.exception;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.http.HttpStatus;

@EqualsAndHashCode(callSuper = true)
@Data
@AllArgsConstructor
public class UserNotActiveException extends RuntimeException{
    private final String message;
    private final HttpStatus httpStatus=HttpStatus.BAD_REQUEST;
}
