package az.edurive.course.exception;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.http.HttpStatus;

@EqualsAndHashCode(callSuper = true)
@AllArgsConstructor
@Data
public class IncorrectPasswordException extends RuntimeException{

    private final HttpStatus httpStatus=HttpStatus.BAD_REQUEST;
    private final String message;

}
