package az.edurive.course.exception;

import az.edurive.course.model.dto.response.ErrorResponse;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.time.LocalDateTime;

/**
 * @author ashraf on 31.01.24
 * @project course
 */
@RestControllerAdvice
public class GlobalExceptionHandler {

  @ExceptionHandler({NotFoundException.class,AlreadyExistsException.class,UnAuthorizedException.class,InvalidAccessException.class,UserNotActiveException.class,IncorrectPasswordException.class})
    public ResponseEntity<ErrorResponse> handleException(Exception exception){
        ErrorResponse errorResponse=new ErrorResponse();
        errorResponse.setErrorMessage(exception.getMessage());
        errorResponse.setErrorCode(getHttpStatus(exception).value());
        errorResponse.setStatus(getHttpStatus(exception));
        errorResponse.setDate(LocalDateTime.now());
        return ResponseEntity.status(errorResponse.getStatus()).body(errorResponse);
    }
    @ExceptionHandler(IllegalArgumentException.class)
    public ResponseEntity<?> handleIllegalArgumentException(IllegalArgumentException e) {
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(e.getMessage());
    }
    private HttpStatus getHttpStatus(Exception ex){
        if(ex instanceof NotFoundException){
            return HttpStatus.NOT_FOUND;
        }else if (ex instanceof AlreadyExistsException){
            return HttpStatus.CONFLICT;
        }else if(ex instanceof UnAuthorizedException){
            return HttpStatus.UNAUTHORIZED;
        }else if(ex instanceof InvalidAccessException || ex instanceof UserNotActiveException){
            return HttpStatus.BAD_REQUEST;
        }
        else{
            return HttpStatus.INTERNAL_SERVER_ERROR;
        }
    }
}
