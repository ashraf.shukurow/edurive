package az.edurive.course.mapper;

import az.edurive.course.model.dto.request.OptionRequest;
import az.edurive.course.model.dto.response.OptionResponse;
import az.edurive.course.model.entity.Option;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring", uses = {QuestionMapper.class})
public interface OptionMapper {
    @Mapping(source = "questionId", target = "question.id")
    @Mapping(source = "correctOption", target = "isCorrectOption")
    Option requestToEntity(OptionRequest optionRequest);

    //    @Mapping(source = "question",target = "questionResponse")
    @Mapping(source = "correctOption", target = "isCorrectOption")
    OptionResponse entityToResponse(Option option);

}
