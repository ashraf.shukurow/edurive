package az.edurive.course.mapper;

import az.edurive.course.model.dto.request.LessonRequest;
import az.edurive.course.model.dto.response.LessonResponse;
import az.edurive.course.model.entity.Lesson;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring",uses = {SubjectMapper.class, VideoMapper.class,QuizMapper.class})
public interface LessonMapper {

    Lesson requestToEntity(LessonRequest lessonRequest);
    @Mapping(target = "subjectResponse",source = "subjects")
    @Mapping(source = "quiz",target = "quizResponse")
//    @Mapping(source = "preViewed",target = "isPreViewed")
    LessonResponse entityToResponse(Lesson lesson);
}
