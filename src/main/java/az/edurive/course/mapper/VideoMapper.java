package az.edurive.course.mapper;

import az.edurive.course.model.dto.request.VideoRequest;
import az.edurive.course.model.dto.response.VideoResponse;
import az.edurive.course.model.entity.Video;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring")
public interface VideoMapper {
    @Mapping(source = "subjectId",target = "subject.id")
    Video requestToEntity(VideoRequest videoRequest);
    @Mapping(source = "subject.id",target = "subjectResponse.id")
    @Mapping(source = "subject.lesson",target = "subjectResponse.lessonResponse")
    @Mapping(source = "requiresPaid",target = "requiresPaid")
    VideoResponse entityToResponse(Video video);
}
