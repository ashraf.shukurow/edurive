package az.edurive.course.mapper;

import az.edurive.course.model.dto.request.UserRequest;
import az.edurive.course.model.dto.response.UserResponse;
import az.edurive.course.model.entity.User;
import lombok.Getter;
import lombok.Setter;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

/**
 * @author ashraf on 31.01.24
 * @project course
 */

@Mapper(componentModel = "spring")
public interface UserMapper {


    User requestToEntity(UserRequest userRequest);

    @Mapping(source = "paid",target = "paid")
    UserResponse entityToResponse(User user);


}
