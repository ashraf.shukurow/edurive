package az.edurive.course.mapper;

import az.edurive.course.model.dto.request.SubjectRequest;
import az.edurive.course.model.dto.response.SubjectResponse;
import az.edurive.course.model.entity.Subject;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring")
public interface SubjectMapper {
    @Mapping(source = "lessonId", target = "lesson.id")
    Subject requestToEntity(SubjectRequest subjectRequest);

    @Mapping(source ="lesson.id",target = "lessonResponse.id")
    @Mapping(source ="video",target = "videoResponse")
    SubjectResponse entityToResponse(Subject subject);

}