package az.edurive.course.mapper;

import az.edurive.course.model.dto.request.QuizRequest;
import az.edurive.course.model.dto.response.QuizResponse;
import az.edurive.course.model.entity.Quiz;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring",uses = {QuestionMapper.class,OptionMapper.class,LessonMapper.class})
public interface QuizMapper {
    @Mapping(source = "lessonId",target = "lesson.id")
    Quiz requestToEntity(QuizRequest quizRequest);

//    @Mapping(source ="lesson.id",target ="lessonId" )
    @Mapping(source = "questions",target = "questionResponses")
    QuizResponse entityToResponse(Quiz quiz);
}
