package az.edurive.course.mapper;

import az.edurive.course.model.dto.request.QuestionRequest;
import az.edurive.course.model.dto.response.QuestionResponse;
import az.edurive.course.model.entity.Question;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring",uses = {QuizMapper.class,OptionMapper.class})
public interface QuestionMapper {
    @Mapping(source = "quizId",target = "quiz.id")
    Question requestToEntity(QuestionRequest questionRequest);
//    @Mapping(source = "quiz.id",target = "quizId")
    @Mapping(source = "options",target = "optionResponses")
    QuestionResponse entityToResponse(Question question);
}
