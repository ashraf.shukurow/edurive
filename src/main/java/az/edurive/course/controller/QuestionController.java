package az.edurive.course.controller;

import az.edurive.course.model.dto.request.QuestionRequest;
import az.edurive.course.model.dto.request.QuizRequest;
import az.edurive.course.model.dto.response.QuestionResponse;
import az.edurive.course.model.dto.response.QuizResponse;
import az.edurive.course.service.QuestionService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/question/")
@CrossOrigin("*")
@Tag(name = "Question", description = "Question Apis")
public class QuestionController {
    private final QuestionService questionService;

    @Operation(
            summary = "Adding Question",
            description = "Adding Question for QuestionRequest"
    )
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", content = {@Content(schema = @Schema(implementation = QuestionRequest.class), mediaType = "app/json")}),
            @ApiResponse(responseCode = "400", content = {@Content(schema = @Schema())})
    })
    @PostMapping("/")
    public void createQuestion(@RequestBody QuestionRequest questionRequest) {
        questionService.createQuestion(questionRequest);
    }
    @Operation(
            summary = "Retrieve a Question by Id",
            description = "Get a Question object by specifying its id. The response is Question object with id"
    )
    @ApiResponses({
            @ApiResponse(responseCode = "200", content = {@Content(schema = @Schema(implementation = QuestionResponse.class), mediaType = "application/json")}),
            @ApiResponse(responseCode = "404", content = {@Content(schema = @Schema())}),
            @ApiResponse(responseCode = "500", content = {@Content(schema = @Schema())})})
    @GetMapping("/{id}")
    public ResponseEntity<QuestionResponse> getQuestionById(@PathVariable Long id) {
        return ResponseEntity.ok(questionService.getQuestionById(id));
    }
    @Operation(
            summary = "Retrieve Questions",
            description = "Get  Question objects ")
    @ApiResponses({
            @ApiResponse(responseCode = "200", content = {@Content(schema = @Schema(implementation = List.class), mediaType = "application/json")}),
            @ApiResponse(responseCode = "404", content = {@Content(schema = @Schema())}),
            @ApiResponse(responseCode = "500", content = {@Content(schema = @Schema())})})
    @GetMapping("/")
    public ResponseEntity<List<QuestionResponse>> getAllQuestion() {
        return ResponseEntity.ok(questionService.getAllQuestion());
    }
    @Operation(
            summary = "Update a Question by Id",
            description = "Update a Question object by specifying its id. The response is Question object with updated version")
    @ApiResponses({
            @ApiResponse(responseCode = "200", content = {@Content(schema = @Schema(implementation = String.class), mediaType = "application/json")}),
            @ApiResponse(responseCode = "404", content = {@Content(schema = @Schema())}),
            @ApiResponse(responseCode = "500", content = {@Content(schema = @Schema())})})

    @PutMapping("/{id}")
    public ResponseEntity<String> updateQuestionById(@RequestBody QuestionRequest questionRequest, @PathVariable Long id) {
        questionService.updateQuestion(questionRequest, id);
        return ResponseEntity.status(HttpStatus.CREATED).body("Question updated successfully");
    }
    @Operation(
            summary = "delete Question",
            description = "delete Question by id")
    @ApiResponses({
            @ApiResponse(responseCode = "200", content = {@Content(schema = @Schema(), mediaType = "application/json")}),
            @ApiResponse(responseCode = "404", content = {@Content(schema = @Schema())}),
            @ApiResponse(responseCode = "500", content = {@Content(schema = @Schema())})})
    @DeleteMapping("/{id}")
    public void deleteQuestionById(@PathVariable Long id){
        questionService.deleteQuestion(id);

    }
}
