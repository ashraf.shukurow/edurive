package az.edurive.course.controller;

import az.edurive.course.service.VerifyAccountService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/v1/verify")
@RequiredArgsConstructor
@CrossOrigin("*")
public class VerifyController {
    private final VerifyAccountService verifyAccountService;
    @GetMapping("/{token}")
    public ResponseEntity<String> verify(@PathVariable String token){
        verifyAccountService.verify(token);
        return ResponseEntity.ok("Email successfully verified");
    }

    @PutMapping("/activated")
    public boolean isActivated(@RequestParam String gmail){
        return verifyAccountService.isActivated(gmail);
    }


}
