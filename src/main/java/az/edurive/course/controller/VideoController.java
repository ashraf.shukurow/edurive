package az.edurive.course.controller;

import az.edurive.course.model.dto.request.UserRequest;
import az.edurive.course.model.dto.request.VideoRequest;
import az.edurive.course.model.dto.response.UserResponse;
import az.edurive.course.model.dto.response.VideoResponse;
import az.edurive.course.service.VideoService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.UUID;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/video")
@Tag(name = "Video",description = "Video Apis")
@CrossOrigin("*")
public class VideoController {
    private final VideoService videoService;
    @Operation(
            summary = "Adding Video",
            description = "Adding video for VideoRequest"
    )
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200",content = {@Content(schema = @Schema(implementation = VideoRequest.class),mediaType = "app/json")}),
            @ApiResponse(responseCode = "400", content ={ @Content(schema = @Schema())})
    })
    @PostMapping("/")
    public ResponseEntity<String> createVideo(VideoRequest videoRequest, @RequestParam MultipartFile videoFile) throws IOException {
        videoService.createVideo(videoRequest, videoFile);
        return ResponseEntity.status(HttpStatus.CREATED).body("Video created successfully");
    }

    @Operation(
            summary = "Retrieve Videos",
            description = "Get  Videos objects ")
    @ApiResponses({
            @ApiResponse(responseCode = "200", content = { @Content(schema = @Schema(implementation = List.class), mediaType = "application/json") }),
            @ApiResponse(responseCode = "404", content = { @Content(schema = @Schema()) }),
            @ApiResponse(responseCode = "500", content = { @Content(schema = @Schema()) }) })
    @GetMapping("/")
    public ResponseEntity<List<VideoResponse>> getAll() {
        return ResponseEntity.ok(videoService.getAllVideo());
    }
    @Operation(
            summary = "Retrieve a Video by Id",
            description = "Get a Video object by specifying its id. The response is User object with id"
    )
    @ApiResponses({
            @ApiResponse(responseCode = "200", content = { @Content(schema = @Schema(implementation = VideoResponse.class), mediaType = "application/json") }),
            @ApiResponse(responseCode = "404", content = { @Content(schema = @Schema()) }),
            @ApiResponse(responseCode = "500", content = { @Content(schema = @Schema()) }) })
    @GetMapping("/{id}/{userId}")
    public ResponseEntity<VideoResponse> getVideoById(@PathVariable Long id,@PathVariable Long userId){
        return ResponseEntity.ok(videoService.getVideoById(id,userId));
    }
    @Operation(
            summary = "Delete a Video by Id",
            description = "Delete a Video object by specifying its id."
    )
    @ApiResponses({
            @ApiResponse(responseCode = "200", content = { @Content(schema = @Schema(implementation = VideoResponse.class), mediaType = "application/json") }),
            @ApiResponse(responseCode = "404", content = { @Content(schema = @Schema()) }),
            @ApiResponse(responseCode = "500", content = { @Content(schema = @Schema()) }) })
    @DeleteMapping("/{id}")
    public void deleteVideo(@PathVariable Long id){
        videoService.deleteVideoById(id);

    }
}
