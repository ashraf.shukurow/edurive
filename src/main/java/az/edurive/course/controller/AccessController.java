package az.edurive.course.controller;

import az.edurive.course.model.dto.request.LessonRequest;
import az.edurive.course.service.AccessService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/access")
@Tag(name = "Access",description = "Access Apis")
@CrossOrigin("*")
public class AccessController {
    private final AccessService accessService;
    @Operation(
            summary = "Give Access To user",
            description = "for giving access you should enter as parameter userId,and VideoId"
    )
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200",content = {@Content(schema = @Schema(implementation = Integer.class),mediaType = "app/json")}),
            @ApiResponse(responseCode = "400", content ={ @Content(schema = @Schema())})
    })
    @PostMapping("/{userId}")
    public ResponseEntity<String> giveAccess(@PathVariable Long userId) {
        return ResponseEntity.ok( accessService.giveAccess(userId));

    }
    @Operation(
            summary = "Revoke Access from user",
            description = "For revoking access, you should enter the userId as a parameter"
    )
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", content = {@Content(schema = @Schema(implementation = Integer.class), mediaType = "app/json")}),
            @ApiResponse(responseCode = "400", content = {@Content(schema = @Schema())})
    })
    @PutMapping("/{userId}")
    public ResponseEntity<String> revokeAccess(@PathVariable Long userId) {
        return ResponseEntity.ok(accessService.revokeAccess(userId));
    }

}
