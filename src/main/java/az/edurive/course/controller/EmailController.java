package az.edurive.course.controller;

import az.edurive.course.model.dto.request.UserRequest;
import az.edurive.course.service.EmailService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequiredArgsConstructor
@RequestMapping("/email")
public class EmailController {

    private final EmailService emailService;

    @PostMapping("/")
    public void sendEmail(@RequestBody UserRequest userRequest){
        emailService.sendMailToAdmin(userRequest);
    }
}
