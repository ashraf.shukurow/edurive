package az.edurive.course.controller;

import az.edurive.course.model.dto.request.OptionRequest;
import az.edurive.course.model.dto.request.QuestionRequest;
import az.edurive.course.model.dto.response.OptionResponse;
import az.edurive.course.model.dto.response.QuestionResponse;
import az.edurive.course.service.OptionService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/v1/option/")
@RequiredArgsConstructor
@Tag(name = "Option", description = "Option Apis")
@CrossOrigin("*")
public class OptionController {
    private final OptionService optionService;
    @Operation(
            summary = "Adding Option",
            description = "Adding Option for OptionRequest"
    )
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", content = {@Content(schema = @Schema(implementation = OptionRequest.class), mediaType = "app/json")}),
            @ApiResponse(responseCode = "400", content = {@Content(schema = @Schema())})
    })
    @PostMapping("/")
    public void createOption(@RequestBody OptionRequest optionRequest){
        optionService.createOption(optionRequest);
    }
    @Operation(
            summary = "Retrieve a Option by Id",
            description = "Get a Option object by specifying its id. The response is Option object with id"
    )
    @ApiResponses({
            @ApiResponse(responseCode = "200", content = {@Content(schema = @Schema(implementation = QuestionResponse.class), mediaType = "application/json")}),
            @ApiResponse(responseCode = "404", content = {@Content(schema = @Schema())}),
            @ApiResponse(responseCode = "500", content = {@Content(schema = @Schema())})})
    @GetMapping("/{id}")
    public ResponseEntity<OptionResponse> getOptionById(@PathVariable Long id){
        return ResponseEntity.ok(optionService.getOptionById(id));
    }
    @Operation(
            summary = "Retrieve Options",
            description = "Get  Option objects ")
    @ApiResponses({
            @ApiResponse(responseCode = "200", content = {@Content(schema = @Schema(implementation = List.class), mediaType = "application/json")}),
            @ApiResponse(responseCode = "404", content = {@Content(schema = @Schema())}),
            @ApiResponse(responseCode = "500", content = {@Content(schema = @Schema())})})
    @GetMapping("/")
    public ResponseEntity<List<OptionResponse>> getAllOption(){
        return ResponseEntity.ok(optionService.getAllOption());
    }
    @Operation(
            summary = "Update a Option by Id",
            description = "Update a Option object by specifying its id. The response is Option object with updated version")
    @ApiResponses({
            @ApiResponse(responseCode = "200", content = {@Content(schema = @Schema(implementation = String.class), mediaType = "application/json")}),
            @ApiResponse(responseCode = "404", content = {@Content(schema = @Schema())}),
            @ApiResponse(responseCode = "500", content = {@Content(schema = @Schema())})})
    @PutMapping("/{id}")
    public ResponseEntity<String> updateOptionById(@RequestBody OptionRequest optionRequest,@PathVariable Long id){
        optionService.updateOption(optionRequest,id);
        return ResponseEntity.status(HttpStatus.CREATED).body("Option updated successfully");
    }
    @Operation(
            summary = "delete Option",
            description = "delete Option by id")
    @ApiResponses({
            @ApiResponse(responseCode = "200", content = {@Content(schema = @Schema(), mediaType = "application/json")}),
            @ApiResponse(responseCode = "404", content = {@Content(schema = @Schema())}),
            @ApiResponse(responseCode = "500", content = {@Content(schema = @Schema())})})
    @DeleteMapping("/{id}")
    public void deleteOption(@PathVariable Long id){
        optionService.deleteOption(id);
    }

}
