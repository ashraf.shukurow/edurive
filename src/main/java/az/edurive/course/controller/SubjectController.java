package az.edurive.course.controller;

import az.edurive.course.model.dto.request.SubjectRequest;
import az.edurive.course.model.dto.response.SubjectResponse;
import az.edurive.course.service.SubjectService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/subject")
@Tag(name = "Subject", description = "Subject Apis")
@CrossOrigin("*")
public class SubjectController {
    private final SubjectService subjectService;

    @Operation(
            summary = "Adding Subject",
            description = "Adding Subject for SubjectRequest"
    )
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", content = {@Content(schema = @Schema(implementation = SubjectRequest.class), mediaType = "app/json")}),
            @ApiResponse(responseCode = "400", content = {@Content(schema = @Schema())})
    })
    @PostMapping("/")
    public void createSubject(@RequestBody SubjectRequest subjectRequest) {
        subjectService.addSubject(subjectRequest);
    }

    @Operation(
            summary = "Retrieve a Subject by Id",
            description = "Get a Subject object by specifying its id. The response is Subject object with id"
    )
    @ApiResponses({
            @ApiResponse(responseCode = "200", content = {@Content(schema = @Schema(implementation = SubjectResponse.class), mediaType = "application/json")}),
            @ApiResponse(responseCode = "404", content = {@Content(schema = @Schema())}),
            @ApiResponse(responseCode = "500", content = {@Content(schema = @Schema())})})
    @GetMapping("/{id}")
    public ResponseEntity<SubjectResponse> getSubjectById(@PathVariable Long id) {
        return ResponseEntity.ok(subjectService.getSubjectById(id));
    }

    @Operation(
            summary = "Retrieve Subjects",
            description = "Get  Subjects objects ")
    @ApiResponses({
            @ApiResponse(responseCode = "200", content = {@Content(schema = @Schema(implementation = List.class), mediaType = "application/json")}),
            @ApiResponse(responseCode = "404", content = {@Content(schema = @Schema())}),
            @ApiResponse(responseCode = "500", content = {@Content(schema = @Schema())})})

    @GetMapping("/")
    public ResponseEntity<List<SubjectResponse>> getAllSubject() {
        return ResponseEntity.ok(subjectService.getAllSubject());
    }

    @Operation(
            summary = "Update a Subject by Id",
            description = "Update a Subject object by specifying its id. The response is Subject object with updated version")
    @ApiResponses({
            @ApiResponse(responseCode = "200", content = {@Content(schema = @Schema(implementation = String.class), mediaType = "application/json")}),
            @ApiResponse(responseCode = "404", content = {@Content(schema = @Schema())}),
            @ApiResponse(responseCode = "500", content = {@Content(schema = @Schema())})})
    @PutMapping("/{id}")
    public ResponseEntity<String> update(@RequestBody SubjectRequest subjectRequest, @PathVariable Long id) {
        subjectService.updateSubject(subjectRequest, id);
        return ResponseEntity.status(HttpStatus.CREATED).body("subject updated successfully");
    }

    @Operation(
            summary = "Retrieve Subjects",
            description = "Get  Subjects objects with Video")
    @ApiResponses({
            @ApiResponse(responseCode = "200", content = {@Content(schema = @Schema(implementation = List.class), mediaType = "application/json")}),
            @ApiResponse(responseCode = "404", content = {@Content(schema = @Schema())}),
            @ApiResponse(responseCode = "500", content = {@Content(schema = @Schema())})})
    @GetMapping("/lesson/{lessonId}")
    public ResponseEntity<List<SubjectResponse>> getAllSubjectByLessonId(@PathVariable Long lessonId) {
        return ResponseEntity.ok(subjectService.getAllSubjectByLessonId(lessonId));
    }

    @Operation(
            summary = "delete Subject",
            description = "delete subject by id")
    @ApiResponses({
            @ApiResponse(responseCode = "200", content = {@Content(schema = @Schema(), mediaType = "application/json")}),
            @ApiResponse(responseCode = "404", content = {@Content(schema = @Schema())}),
            @ApiResponse(responseCode = "500", content = {@Content(schema = @Schema())})})
    @DeleteMapping("/{id}")
    public void deleteSubject(@PathVariable Long id) {
        subjectService.deleteSubjectById(id);

    }

}
