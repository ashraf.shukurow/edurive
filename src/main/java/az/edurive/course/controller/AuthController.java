package az.edurive.course.controller;

import az.edurive.course.model.dto.request.LoginRequest;
import az.edurive.course.model.dto.request.UserRequest;
import az.edurive.course.model.dto.response.AuthResponse;
import az.edurive.course.model.dto.response.TokenResponse;
import az.edurive.course.model.dto.response.UserResponse;
import az.edurive.course.service.AuthService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.*;

@RestController
@RequiredArgsConstructor
@RequestMapping("/auth")
@CrossOrigin("*")
@Tag(name = "Authentication", description = "Authentication Apis")
public class AuthController {
    private final AuthService authService;

    @Operation(summary = "Login user with LoginRequest", description = "Login user with LoginRequest")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", content = {@Content(schema = @Schema(implementation = LoginRequest.class), mediaType = "app/json")}),
            @ApiResponse(responseCode = "400", content = {@Content(schema = @Schema())})})
    @PostMapping("/login")
    public ResponseEntity<AuthResponse> login(@Valid @RequestBody LoginRequest request) {
        return ResponseEntity.accepted().body(authService.login(request));
    }

    @Operation(summary = "Refresh bearer token", description = "Refresh bearer token")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", content = {@Content(schema = @Schema(implementation = UserDetails.class), mediaType = "app/json")}),
            @ApiResponse(responseCode = "400", content = {@Content(schema = @Schema())})})

    @PostMapping("/refresh")
    public ResponseEntity<TokenResponse> refreshToken(@AuthenticationPrincipal UserDetails userDetails) {
        return ResponseEntity.ok(authService.refreshToken(userDetails));
    }

    @Operation(summary = "Adding user with UserRequest", description = "Adding user for UserRequest")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", content = {@Content(schema = @Schema(implementation = UserRequest.class), mediaType = "app/json")}),
            @ApiResponse(responseCode = "400", content = {@Content(schema = @Schema())})})

    @PostMapping("/registration")
    public ResponseEntity<UserResponse> userRegistration(@Valid @RequestBody UserRequest userRequest) {
       return ResponseEntity.ok(authService.registration(userRequest));
    }
    @PutMapping("/forgot-password")
    public ResponseEntity<String> forgotPassword(@RequestParam String email){
        return ResponseEntity.ok(authService.forgotPassword(email));
    }

    @PutMapping("/set-password")
    public ResponseEntity<String> setPassword(@RequestParam String email,@RequestHeader String newPassword){
        return ResponseEntity.ok(authService.setPassword(email,newPassword));
    }
}
