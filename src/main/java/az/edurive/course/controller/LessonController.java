package az.edurive.course.controller;

import az.edurive.course.model.dto.request.LessonRequest;
import az.edurive.course.model.dto.request.UserRequest;
import az.edurive.course.model.dto.response.LessonResponse;
import az.edurive.course.model.dto.response.SubjectResponse;
import az.edurive.course.model.dto.response.UserResponse;
import az.edurive.course.service.LessonService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.apache.coyote.Response;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.parameters.P;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/lesson")
@Tag(name = "Lesson",description = "Lesson Apis")
@CrossOrigin("*")
public class LessonController {
    private final LessonService lessonService;
    @Operation(
            summary = "Adding lesson",
            description = "Adding lesson for LessonRequest"
    )
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200",content = {@Content(schema = @Schema(implementation = LessonRequest.class),mediaType = "app/json")}),
            @ApiResponse(responseCode = "400", content ={ @Content(schema = @Schema())})
    })
    @PostMapping("/")
    public void createLesson(@RequestBody LessonRequest lessonRequest) {
        lessonService.createLesson(lessonRequest);
    }
    @Operation(
            summary = "Retrieve Lessons",
            description = "Get  Lessons objects ")
    @ApiResponses({
            @ApiResponse(responseCode = "200", content = { @Content(schema = @Schema(implementation = List.class), mediaType = "application/json") }),
            @ApiResponse(responseCode = "404", content = { @Content(schema = @Schema()) }),
            @ApiResponse(responseCode = "500", content = { @Content(schema = @Schema()) }) })
    @GetMapping("/")
    public ResponseEntity<List<LessonResponse>> getAllLesson() {
        return ResponseEntity.ok(lessonService.getAllLesson());
    }
    @Operation(
            summary = "Retrieve a Lesson by Id",
            description = "Get a Lesson object by specifying its id. The response is Lesson object with id"
    )
    @ApiResponses({
            @ApiResponse(responseCode = "200", content = { @Content(schema = @Schema(implementation = LessonResponse.class), mediaType = "application/json") }),
            @ApiResponse(responseCode = "404", content = { @Content(schema = @Schema()) }),
            @ApiResponse(responseCode = "500", content = { @Content(schema = @Schema()) }) })
    @GetMapping("/{id}")
    public ResponseEntity<LessonResponse> getLessonById(@PathVariable Long id) {
        return ResponseEntity.ok(lessonService.getLessonById(id));
    }
    @Operation(
            summary = "Update a Lesson by Id",
            description = "Update a Lesson object by specifying its id. The response is Lesson object with updated version")
    @ApiResponses({
            @ApiResponse(responseCode = "200", content = { @Content(schema = @Schema(implementation = LessonResponse.class), mediaType = "application/json") }),
            @ApiResponse(responseCode = "404", content = { @Content(schema = @Schema()) }),
            @ApiResponse(responseCode = "500", content = { @Content(schema = @Schema()) }) })
    @PutMapping("/{id}")
    public void updateLesson(@RequestBody LessonRequest lessonRequest, @PathVariable Long id) {
        lessonService.updateLesson(lessonRequest, id);
    }

    @Operation(
            summary = "Delete a Lesson by Id",
            description = "Delete a Lesson object by specifying its id. The response is Lesson object with deleted version")
    @ApiResponses({
            @ApiResponse(responseCode = "200", content = { @Content(schema = @Schema(implementation = LessonResponse.class), mediaType = "application/json") }),
            @ApiResponse(responseCode = "404", content = { @Content(schema = @Schema()) }),
            @ApiResponse(responseCode = "500", content = { @Content(schema = @Schema()) }) })
    @DeleteMapping("/{id}")
    public void deleteLesson(@PathVariable Long id){
        lessonService.deleteLesson(id);
    }
    @Operation(
            summary = "Retrieve All Lesson by UserId",
            description = "Get All Lesson object by specifying User id. The response is Lessons object with Userid"
    )
    @ApiResponses({
            @ApiResponse(responseCode = "200", content = { @Content(schema = @Schema(implementation = LessonResponse.class), mediaType = "application/json") }),
            @ApiResponse(responseCode = "404", content = { @Content(schema = @Schema()) }),
            @ApiResponse(responseCode = "500", content = { @Content(schema = @Schema()) }) })
    @GetMapping("/user/{userId}")
    public ResponseEntity<List<LessonResponse>> getAllLessonByUserId(@PathVariable Long userId){
        return ResponseEntity.ok(lessonService.getAllLessonByUserId(userId));
    }

}
