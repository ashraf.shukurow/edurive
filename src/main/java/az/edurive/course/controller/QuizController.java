package az.edurive.course.controller;

import az.edurive.course.model.dto.request.QuizRequest;
import az.edurive.course.model.dto.response.QuizResponse;
import az.edurive.course.service.QuizService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/quiz/")
@Tag(name = "Quiz", description = "Quiz Apis")
@CrossOrigin("*")
public class QuizController {
    private final QuizService quizService;
    @Operation(
            summary = "Adding Quiz",
            description = "Adding Quiz for QuizRequest"
    )
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", content = {@Content(schema = @Schema(implementation = QuizRequest.class), mediaType = "app/json")}),
            @ApiResponse(responseCode = "400", content = {@Content(schema = @Schema())})
    })
    @PostMapping("/")
    public void createQuiz(@RequestBody QuizRequest quizRequest){
        quizService.createQuiz(quizRequest);
    }
    @Operation(
            summary = "Retrieve a Quiz by Id",
            description = "Get a Quiz object by specifying its id. The response is Quiz object with id"
    )
    @ApiResponses({
            @ApiResponse(responseCode = "200", content = {@Content(schema = @Schema(implementation = QuizResponse.class), mediaType = "application/json")}),
            @ApiResponse(responseCode = "404", content = {@Content(schema = @Schema())}),
            @ApiResponse(responseCode = "500", content = {@Content(schema = @Schema())})})
    @GetMapping("/{id}")
    public ResponseEntity<QuizResponse> getQuizById(@PathVariable Long id){
      return  ResponseEntity.ok(quizService.getQuizById(id));
    }
    @Operation(
            summary = "Retrieve Quizies",
            description = "Get  Quiz objects ")
    @ApiResponses({
            @ApiResponse(responseCode = "200", content = {@Content(schema = @Schema(implementation = List.class), mediaType = "application/json")}),
            @ApiResponse(responseCode = "404", content = {@Content(schema = @Schema())}),
            @ApiResponse(responseCode = "500", content = {@Content(schema = @Schema())})})
    @GetMapping("/")
    public ResponseEntity<List<QuizResponse>> getAllQuiz(){
        return ResponseEntity.ok(quizService.getAllQuiz());
    }
    @Operation(
            summary = "Update a Quiz by Id",
            description = "Update a Quiz object by specifying its id. The response is Quiz object with updated version")
    @ApiResponses({
            @ApiResponse(responseCode = "200", content = {@Content(schema = @Schema(implementation = String.class), mediaType = "application/json")}),
            @ApiResponse(responseCode = "404", content = {@Content(schema = @Schema())}),
            @ApiResponse(responseCode = "500", content = {@Content(schema = @Schema())})})
    @PutMapping("/{id}")
    public ResponseEntity<String> updateQuizById(@RequestBody QuizRequest quizRequest,@PathVariable Long id){
         quizService.updateQuiz(quizRequest,id);
         return ResponseEntity.status(HttpStatus.CREATED).body("Quiz updated successfully");
    }
    @Operation(
            summary = "delete Quiz",
            description = "delete Quiz by id")
    @ApiResponses({
            @ApiResponse(responseCode = "200", content = {@Content(schema = @Schema(), mediaType = "application/json")}),
            @ApiResponse(responseCode = "404", content = {@Content(schema = @Schema())}),
            @ApiResponse(responseCode = "500", content = {@Content(schema = @Schema())})})
    @DeleteMapping("/{id}")
    public void deleteQuizById(@PathVariable Long id){
        quizService.deleteQuiz(id);
    }
}
