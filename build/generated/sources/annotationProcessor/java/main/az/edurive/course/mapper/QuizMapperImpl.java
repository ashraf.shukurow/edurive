package az.edurive.course.mapper;

import az.edurive.course.model.dto.request.QuizRequest;
import az.edurive.course.model.dto.response.QuestionResponse;
import az.edurive.course.model.dto.response.QuizResponse;
import az.edurive.course.model.dto.response.QuizResponse.QuizResponseBuilder;
import az.edurive.course.model.entity.Lesson;
import az.edurive.course.model.entity.Question;
import az.edurive.course.model.entity.Quiz;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.processing.Generated;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2024-10-16T00:12:47+0400",
    comments = "version: 1.4.2.Final, compiler: IncrementalProcessingEnvironment from gradle-language-java-8.5.jar, environment: Java 17.0.12 (Amazon.com Inc.)"
)
@Component
public class QuizMapperImpl implements QuizMapper {

    @Autowired
    private QuestionMapper questionMapper;

    @Override
    public Quiz requestToEntity(QuizRequest quizRequest) {
        if ( quizRequest == null ) {
            return null;
        }

        Quiz quiz = new Quiz();

        quiz.setLesson( quizRequestToLesson( quizRequest ) );
        quiz.setQuizName( quizRequest.getQuizName() );

        return quiz;
    }

    @Override
    public QuizResponse entityToResponse(Quiz quiz) {
        if ( quiz == null ) {
            return null;
        }

        QuizResponseBuilder quizResponse = QuizResponse.builder();

        quizResponse.questionResponses( questionListToQuestionResponseList( quiz.getQuestions() ) );
        quizResponse.id( quiz.getId() );
        quizResponse.quizName( quiz.getQuizName() );

        return quizResponse.build();
    }

    protected Lesson quizRequestToLesson(QuizRequest quizRequest) {
        if ( quizRequest == null ) {
            return null;
        }

        Lesson lesson = new Lesson();

        lesson.setId( quizRequest.getLessonId() );

        return lesson;
    }

    protected List<QuestionResponse> questionListToQuestionResponseList(List<Question> list) {
        if ( list == null ) {
            return null;
        }

        List<QuestionResponse> list1 = new ArrayList<QuestionResponse>( list.size() );
        for ( Question question : list ) {
            list1.add( questionMapper.entityToResponse( question ) );
        }

        return list1;
    }
}
