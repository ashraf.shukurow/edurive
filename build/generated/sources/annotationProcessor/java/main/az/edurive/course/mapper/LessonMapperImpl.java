package az.edurive.course.mapper;

import az.edurive.course.model.dto.request.LessonRequest;
import az.edurive.course.model.dto.response.LessonResponse;
import az.edurive.course.model.dto.response.LessonResponse.LessonResponseBuilder;
import az.edurive.course.model.dto.response.SubjectResponse;
import az.edurive.course.model.entity.Lesson;
import az.edurive.course.model.entity.Subject;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.processing.Generated;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2024-10-16T00:12:48+0400",
    comments = "version: 1.4.2.Final, compiler: IncrementalProcessingEnvironment from gradle-language-java-8.5.jar, environment: Java 17.0.12 (Amazon.com Inc.)"
)
@Component
public class LessonMapperImpl implements LessonMapper {

    @Autowired
    private SubjectMapper subjectMapper;
    @Autowired
    private QuizMapper quizMapper;

    @Override
    public Lesson requestToEntity(LessonRequest lessonRequest) {
        if ( lessonRequest == null ) {
            return null;
        }

        Lesson lesson = new Lesson();

        lesson.setLessonName( lessonRequest.getLessonName() );

        return lesson;
    }

    @Override
    public LessonResponse entityToResponse(Lesson lesson) {
        if ( lesson == null ) {
            return null;
        }

        LessonResponseBuilder lessonResponse = LessonResponse.builder();

        lessonResponse.subjectResponse( subjectListToSubjectResponseList( lesson.getSubjects() ) );
        lessonResponse.quizResponse( quizMapper.entityToResponse( lesson.getQuiz() ) );
        lessonResponse.id( lesson.getId() );
        lessonResponse.lessonName( lesson.getLessonName() );

        return lessonResponse.build();
    }

    protected List<SubjectResponse> subjectListToSubjectResponseList(List<Subject> list) {
        if ( list == null ) {
            return null;
        }

        List<SubjectResponse> list1 = new ArrayList<SubjectResponse>( list.size() );
        for ( Subject subject : list ) {
            list1.add( subjectMapper.entityToResponse( subject ) );
        }

        return list1;
    }
}
