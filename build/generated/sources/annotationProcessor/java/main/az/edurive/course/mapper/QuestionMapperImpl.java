package az.edurive.course.mapper;

import az.edurive.course.model.dto.request.QuestionRequest;
import az.edurive.course.model.dto.response.OptionResponse;
import az.edurive.course.model.dto.response.QuestionResponse;
import az.edurive.course.model.dto.response.QuestionResponse.QuestionResponseBuilder;
import az.edurive.course.model.entity.Option;
import az.edurive.course.model.entity.Question;
import az.edurive.course.model.entity.Quiz;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import javax.annotation.processing.Generated;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2024-10-16T00:12:48+0400",
    comments = "version: 1.4.2.Final, compiler: IncrementalProcessingEnvironment from gradle-language-java-8.5.jar, environment: Java 17.0.12 (Amazon.com Inc.)"
)
@Component
public class QuestionMapperImpl implements QuestionMapper {

    @Autowired
    private OptionMapper optionMapper;

    @Override
    public Question requestToEntity(QuestionRequest questionRequest) {
        if ( questionRequest == null ) {
            return null;
        }

        Question question = new Question();

        question.setQuiz( questionRequestToQuiz( questionRequest ) );
        question.setText( questionRequest.getText() );

        return question;
    }

    @Override
    public QuestionResponse entityToResponse(Question question) {
        if ( question == null ) {
            return null;
        }

        QuestionResponseBuilder questionResponse = QuestionResponse.builder();

        questionResponse.optionResponses( optionSetToOptionResponseList( question.getOptions() ) );
        questionResponse.id( question.getId() );
        questionResponse.text( question.getText() );

        return questionResponse.build();
    }

    protected Quiz questionRequestToQuiz(QuestionRequest questionRequest) {
        if ( questionRequest == null ) {
            return null;
        }

        Quiz quiz = new Quiz();

        quiz.setId( questionRequest.getQuizId() );

        return quiz;
    }

    protected List<OptionResponse> optionSetToOptionResponseList(Set<Option> set) {
        if ( set == null ) {
            return null;
        }

        List<OptionResponse> list = new ArrayList<OptionResponse>( set.size() );
        for ( Option option : set ) {
            list.add( optionMapper.entityToResponse( option ) );
        }

        return list;
    }
}
