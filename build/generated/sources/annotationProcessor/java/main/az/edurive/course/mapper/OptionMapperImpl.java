package az.edurive.course.mapper;

import az.edurive.course.model.dto.request.OptionRequest;
import az.edurive.course.model.dto.response.OptionResponse;
import az.edurive.course.model.dto.response.OptionResponse.OptionResponseBuilder;
import az.edurive.course.model.entity.Option;
import az.edurive.course.model.entity.Option.OptionBuilder;
import az.edurive.course.model.entity.Question;
import javax.annotation.processing.Generated;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2024-10-16T00:12:48+0400",
    comments = "version: 1.4.2.Final, compiler: IncrementalProcessingEnvironment from gradle-language-java-8.5.jar, environment: Java 17.0.12 (Amazon.com Inc.)"
)
@Component
public class OptionMapperImpl implements OptionMapper {

    @Override
    public Option requestToEntity(OptionRequest optionRequest) {
        if ( optionRequest == null ) {
            return null;
        }

        OptionBuilder option = Option.builder();

        option.question( optionRequestToQuestion( optionRequest ) );
        option.isCorrectOption( optionRequest.isCorrectOption() );
        option.optionText( optionRequest.getOptionText() );

        return option.build();
    }

    @Override
    public OptionResponse entityToResponse(Option option) {
        if ( option == null ) {
            return null;
        }

        OptionResponseBuilder optionResponse = OptionResponse.builder();

        optionResponse.isCorrectOption( option.isCorrectOption() );
        optionResponse.id( option.getId() );
        optionResponse.optionText( option.getOptionText() );

        return optionResponse.build();
    }

    protected Question optionRequestToQuestion(OptionRequest optionRequest) {
        if ( optionRequest == null ) {
            return null;
        }

        Question question = new Question();

        question.setId( optionRequest.getQuestionId() );

        return question;
    }
}
