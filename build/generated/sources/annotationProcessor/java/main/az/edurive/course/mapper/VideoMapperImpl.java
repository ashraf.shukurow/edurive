package az.edurive.course.mapper;

import az.edurive.course.model.dto.request.VideoRequest;
import az.edurive.course.model.dto.response.LessonResponse;
import az.edurive.course.model.dto.response.LessonResponse.LessonResponseBuilder;
import az.edurive.course.model.dto.response.SubjectResponse;
import az.edurive.course.model.dto.response.SubjectResponse.SubjectResponseBuilder;
import az.edurive.course.model.dto.response.VideoResponse;
import az.edurive.course.model.dto.response.VideoResponse.VideoResponseBuilder;
import az.edurive.course.model.entity.Lesson;
import az.edurive.course.model.entity.Subject;
import az.edurive.course.model.entity.Video;
import javax.annotation.processing.Generated;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2024-10-16T00:12:48+0400",
    comments = "version: 1.4.2.Final, compiler: IncrementalProcessingEnvironment from gradle-language-java-8.5.jar, environment: Java 17.0.12 (Amazon.com Inc.)"
)
@Component
public class VideoMapperImpl implements VideoMapper {

    @Override
    public Video requestToEntity(VideoRequest videoRequest) {
        if ( videoRequest == null ) {
            return null;
        }

        Video video = new Video();

        video.setSubject( videoRequestToSubject( videoRequest ) );
        video.setTitle( videoRequest.getTitle() );
        video.setUrl( videoRequest.getUrl() );
        video.setRequiresPaid( videoRequest.isRequiresPaid() );

        return video;
    }

    @Override
    public VideoResponse entityToResponse(Video video) {
        if ( video == null ) {
            return null;
        }

        VideoResponseBuilder videoResponse = VideoResponse.builder();

        videoResponse.subjectResponse( subjectToSubjectResponse( video.getSubject() ) );
        videoResponse.requiresPaid( video.isRequiresPaid() );
        videoResponse.id( video.getId() );
        videoResponse.title( video.getTitle() );
        videoResponse.url( video.getUrl() );

        return videoResponse.build();
    }

    protected Subject videoRequestToSubject(VideoRequest videoRequest) {
        if ( videoRequest == null ) {
            return null;
        }

        Subject subject = new Subject();

        subject.setId( videoRequest.getSubjectId() );

        return subject;
    }

    protected LessonResponse lessonToLessonResponse(Lesson lesson) {
        if ( lesson == null ) {
            return null;
        }

        LessonResponseBuilder lessonResponse = LessonResponse.builder();

        lessonResponse.id( lesson.getId() );
        lessonResponse.lessonName( lesson.getLessonName() );

        return lessonResponse.build();
    }

    protected SubjectResponse subjectToSubjectResponse(Subject subject) {
        if ( subject == null ) {
            return null;
        }

        SubjectResponseBuilder subjectResponse = SubjectResponse.builder();

        subjectResponse.id( subject.getId() );
        subjectResponse.lessonResponse( lessonToLessonResponse( subject.getLesson() ) );
        subjectResponse.subjectName( subject.getSubjectName() );

        return subjectResponse.build();
    }
}
