package az.edurive.course.mapper;

import az.edurive.course.model.dto.request.SubjectRequest;
import az.edurive.course.model.dto.response.LessonResponse;
import az.edurive.course.model.dto.response.LessonResponse.LessonResponseBuilder;
import az.edurive.course.model.dto.response.SubjectResponse;
import az.edurive.course.model.dto.response.SubjectResponse.SubjectResponseBuilder;
import az.edurive.course.model.dto.response.VideoResponse;
import az.edurive.course.model.dto.response.VideoResponse.VideoResponseBuilder;
import az.edurive.course.model.entity.Lesson;
import az.edurive.course.model.entity.Subject;
import az.edurive.course.model.entity.Video;
import javax.annotation.processing.Generated;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2024-10-16T00:12:48+0400",
    comments = "version: 1.4.2.Final, compiler: IncrementalProcessingEnvironment from gradle-language-java-8.5.jar, environment: Java 17.0.12 (Amazon.com Inc.)"
)
@Component
public class SubjectMapperImpl implements SubjectMapper {

    @Override
    public Subject requestToEntity(SubjectRequest subjectRequest) {
        if ( subjectRequest == null ) {
            return null;
        }

        Subject subject = new Subject();

        subject.setLesson( subjectRequestToLesson( subjectRequest ) );
        subject.setSubjectName( subjectRequest.getSubjectName() );

        return subject;
    }

    @Override
    public SubjectResponse entityToResponse(Subject subject) {
        if ( subject == null ) {
            return null;
        }

        SubjectResponseBuilder subjectResponse = SubjectResponse.builder();

        subjectResponse.lessonResponse( lessonToLessonResponse( subject.getLesson() ) );
        subjectResponse.videoResponse( videoToVideoResponse( subject.getVideo() ) );
        subjectResponse.id( subject.getId() );
        subjectResponse.subjectName( subject.getSubjectName() );

        return subjectResponse.build();
    }

    protected Lesson subjectRequestToLesson(SubjectRequest subjectRequest) {
        if ( subjectRequest == null ) {
            return null;
        }

        Lesson lesson = new Lesson();

        lesson.setId( subjectRequest.getLessonId() );

        return lesson;
    }

    protected LessonResponse lessonToLessonResponse(Lesson lesson) {
        if ( lesson == null ) {
            return null;
        }

        LessonResponseBuilder lessonResponse = LessonResponse.builder();

        lessonResponse.id( lesson.getId() );
        lessonResponse.lessonName( lesson.getLessonName() );

        return lessonResponse.build();
    }

    protected VideoResponse videoToVideoResponse(Video video) {
        if ( video == null ) {
            return null;
        }

        VideoResponseBuilder videoResponse = VideoResponse.builder();

        videoResponse.id( video.getId() );
        videoResponse.title( video.getTitle() );
        videoResponse.url( video.getUrl() );
        videoResponse.requiresPaid( video.isRequiresPaid() );

        return videoResponse.build();
    }
}
